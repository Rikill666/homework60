import React from 'react';
import Chat from "./containers/Chat/Chat";


function App() {
  return (
    <div className="App" style={{background: "#373a3c"}}>
        <Chat/>
    </div>
  );
}

export default App;
