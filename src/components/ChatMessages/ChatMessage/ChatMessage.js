import React, {Component} from 'react';
import Moment from "react-moment";

class ChatMessage extends Component {
    shouldComponentUpdate(nextProps, nextState, nextContext) {
        return nextProps.message !== this.props.message;
    }
    render() {
        return (
            <tr>
                <td>
                    <Moment format="D.M.Y. H:mm:ss">
                        {this.props.message.datetime}
                    </Moment></td>
                <td> {this.props.message.author}</td>
                <td> {this.props.message.message}</td>
            </tr>
        );
    }
}

export default ChatMessage;
