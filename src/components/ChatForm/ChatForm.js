import React from 'react';
import {Button, Form, Input} from "reactstrap";
import './ChatForm.css';
const ChatForm = (props) => {
    return (
            <Form onSubmit={(e)=>props.addMessage(e)} className="form-inline mb-3 myForm" id="myForm">
                <div className="form-group">
                    <Input onChange={props.currentAuthor} className="form-control mr-2" id="userName" type="text" placeholder="Имя"/>
                </div>
                <div className="form-group">
                    <Input onChange={props.currentMessage} className="form-control" id="message" type="text" placeholder="Сообщение"/>
                </div>
                <Button type="submit" className="btn btn-primary ml-2">Submit</Button>
            </Form>
    );
};

export default ChatForm;