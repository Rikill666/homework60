import React, {Component} from 'react';
import ChatForm from "../../components/ChatForm/ChatForm";
import {Container} from "reactstrap";
import ChatMessages from "../../components/ChatMessages/ChatMessages";
import axios from 'axios';

class Chat extends Component {
    interval = null;
    state = {
        messages: [],
        lastPostTime: "",
        newMessage: {message: "", author: ""}
    };
    currentMessage = event => {
        let newMessage = {...this.state.newMessage};
        newMessage.message = event.target.value;
        this.setState({newMessage});
    };
    currentAuthor = event => {
        const newMessage = {...this.state.newMessage};
        newMessage.author = event.target.value;
        this.setState({newMessage});
    };

    addMessage = async (e) => {
        e.preventDefault();
        if (this.state.newMessage.message && this.state.newMessage.author) {
            try {
                const data = new URLSearchParams();
                data.set("message", this.state.newMessage.message);
                data.set('author', this.state.newMessage.author);
                await fetch("http://146.185.154.90:8000/messages", {method: 'post', body: data});
                //  await axios.post("http://146.185.154.90:8000/messages", {
                //     message: this.state.newMessage.message,
                //     author: this.state.newMessage.author
                // });
                // console.log(aaa.data);
                document.getElementById('myForm').reset();
                this.setState({newMessage: {message: "", author: ""}});
            } catch (error) {
                alert("Ошибка! Сообщение не отправлено.");
            }
        }
    };
    getMessage = async() =>{
        try {
            let messagesPromise;
            if(!this.state.lastPostTime){
                messagesPromise = await axios.get("http://146.185.154.90:8000/messages");
            }
            else{
                messagesPromise = await axios.get("http://146.185.154.90:8000/messages?datetime=" + this.state.lastPostTime);
            }
            const oldMessages = messagesPromise.data;
            if (oldMessages.length !== 0) {
                const lastPostTime = oldMessages[oldMessages.length - 1].datetime;
                const messages = this.state.messages.concat(oldMessages);
                this.setState({messages, lastPostTime});
            }
        } catch (error) {
            alert("Ошибка!");
        }
    };

    componentDidMount = () =>{
        this.getMessage();
        this.interval = setInterval(async () => {
            await this.getMessage();
        }, 3000);
    };

    componentDidUpdate = (prevProps, prevState, snapshot) => {
        if(this.state !== prevState){
            clearInterval(this.interval);
            this.interval = setInterval(async () => {
                await this.getMessage();
            }, 3000);
            const messagesBlockScroll = document.getElementById('messagesBlock');
            messagesBlockScroll.scrollTop = messagesBlockScroll.scrollHeight;
        }
    };
    render() {
        return (
            <Container>
                <ChatMessages messages={this.state.messages}/>
                <ChatForm addMessage={this.addMessage} currentMessage={this.currentMessage} currentAuthor={this.currentAuthor}/>
            </Container>
        );
    }
}

export default Chat;